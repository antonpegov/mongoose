/**
 * Created by apegov on 18.07.2016.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personSchema = Schema({
    name    : String,
    age     : Number,
    stories : [{ type: Schema.Types.ObjectId, ref: 'Story' }]
}); //схема "человек" c масивом id "историй"
var storySchema = Schema({
    _creator : { type: Schema.Types.ObjectId, ref: 'Person' },
    title    : String,
    fans     : [{ type: Schema.Types.ObjectId, ref: 'Person' }]
}); //схема "история" с id "человека"-автора

var Story  = mongoose.model('Story', storySchema); //модель "история"
var Person = mongoose.model('Person', personSchema); //модель"человек"

mongoose.connect('mongodb://anton:anton@ds023475.mlab.com:23475/mongoose', function(err) {
    if (err)  throw err;
    var aaron = new Person({name:'aaron', age: 100}); //создаем "человека"
    aaron.save(function (err) {
        if (err) throw err;
        var story1 = new Story({
            title: "Про Аарона",
            _creator: aaron._id    //вставляем id Аарона в свойство 'создатель'
        });
        story1.save(function (err) {
            if (err) throw err;
            /*aaron.stories.push(story1._id); //созданную историю прописываем у Арона
            aaron.save(function (err) {
                if (err) throw err;
                mongoose.disconnect();
            });*/ //и сохраняем изменения
            Person.findByIdAndUpdate(
                aaron._id, 
                {$push:{"stories":story1._id}},
                function(err){
                    if (err) throw err;
                    check(function(){
                        cleanup();
                    });                    
                }
            ); // добавляем историю Аарону
        }); //сохраняем историю
    }); //сохраняем его и его историю с перекрёстными ссылками
});
function check(callback) {
    Story.findOne({title: "Про Аарона"})
        .populate('_creator') //на место _creator подставляется результат запроса к Person
        .exec(function (err, story) {
            if (err) throw err;
            console.log('Автор истории %s', story._creator.name);
            callback();
        });
    
} //проверка
function cleanup() {
    Person.remove(function() {
        Story.remove(function(){
            console.log('Disconnecting.');
            mongoose.disconnect();
        });        
    });
} //очистка базы данных