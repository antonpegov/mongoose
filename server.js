/**
 * Created by apegov on 15.07.2016.
 */
var config = {};
config.db = 'mongodb://anton:anton@ds037447.mlab.com:37447/node-patterns';
// ----------- ПОДКЛЮЧЕНИЕ К БД -----------------
mongoose.connection.on("open", function(ref) {
    return console.log("Connected to mongo server!".green);
}).on("error", function(err) {
    console.log("Could not connect to mongo server!".yellow);
    return console.log(err.message.red);
});
try {
    mongoose.connect(config.db);
    var db = mongoose.connection;
    //console.log("Started connection on " + (config.db) + ", waiting for it to open...".grey);
} catch (err) {
    console.log(("Setting up failed to connect to " + config.db).red, err.message);
}

var User = require('./User');
var Post = require('./Post');

// ============================================
// ====== Создаём двух пользователей ==========
// ============================================

var alex = new User({ name: "Alex" });
var joe = new User({ name: "Joe" });

alex.save();
joe.save();

// =========================================
// ============  Создаём пост  ============
// =========================================

var post = new Post({
    title: "Hello World",
    postedBy: alex._id,
    comments: [{
        text: "Nice post!",
        postedBy: joe._id //ObjectId
    }, {
        text: "Thanks :)",
        postedBy: alex._id //ObjectId
    }]
});

post.save(function(error) {
    if (!error) {
        Post.find({})
            .populate('postedBy') //присоединяет коллекцию Users по полю postedBy
            .populate('comments.postedBy') // то же по полю comments
            .exec(function(error, posts) {
                console.log(JSON.stringify(posts, null, "\t"))
            })
    }
});
// Сохранить и вывести все посты, фишка в функции populate(), которая
// собирает информацию из связанных документов при запросе к базе данных
/* =========== Так ПОСТ выглядит в Базе Данных ===========
{
    "_id" : ObjectId("54cd6669d3e0fb1b302e54e6"),
    "title" : "Hello World",
    "postedBy" : ObjectId("54cd6669d3e0fb1b302e54e4"),
    "comments" : [
    {
        "text" : "Nice post!",
        "postedBy" : ObjectId("54cd6669d3e0fb1b302e54e5"),
        "_id" : ObjectId("54cd6669d3e0fb1b302e54e8")
    },
    {
        "text" : "Thanks :)",
        "postedBy" : ObjectId("54cd6669d3e0fb1b302e54e4"),
        "_id" : ObjectId("54cd6669d3e0fb1b302e54e7")
    }
],
    "__v" : 0
}*/
/* ============== Так выглятир результат запроса ===========
[
    {
        "_id": "54cd6669d3e0fb1b302e54e6",
        "title": "Hello World",
        "postedBy": {
            "_id": "54cd6669d3e0fb1b302e54e4",
            "name": "Alex",
            "__v": 0
        },
        "__v": 0,
        "comments": [
            {
                "text": "Nice post!",
                "postedBy": {
                    "_id": "54cd6669d3e0fb1b302e54e5",
                    "name": "Joe",
                    "__v": 0
                },
                "_id": "54cd6669d3e0fb1b302e54e8"
            },
            {
                "text": "Thanks :)",
                "postedBy": {
                    "_id": "54cd6669d3e0fb1b302e54e4",
                    "name": "Alex",
                    "__v": 0
                },
                "_id": "54cd6669d3e0fb1b302e54e7"
            }
        ]
    }
]*/
